package com.p2p.peers.service;

import com.p2p.network.HostSelectionInterceptor;
import com.p2p.network.NetworkService;
import com.p2p.network.RestService;
import com.p2p.peers.model.Peer;
import org.apache.tomcat.util.http.parser.Host;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.PUT;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The type Peer rest service.
 */
@Component
public class PeerRestService extends RestService {

    private PeerApi peerApi;

    @Autowired
    private PeerNetworkService peerNetworkService;
    @Autowired
    private HostSelectionInterceptor hostSelectionInterceptor;

    @Override
    protected void initializeService() {
        this.peerApi = getRestClient().create(PeerApi.class);
    }

    @Override
    protected NetworkService getNetworkService() {
        return peerNetworkService;
    }

    /**
     * Send new peers to peer.
     *
     * @param peer  the peer
     * @param peers the peers
     */
    public void sendNewPeersToPeer(Peer peer, List<Peer> peers) {
        Map<String, Object> requestParameters = new HashMap<>();
        requestParameters.put("peers", peers);
        hostSelectionInterceptor.setHost(Peer.getPeerUrl(peer));
        executeRequest(peerApi.addPeers(requestParameters));

    }

    /**
     * Send updated info to peer.
     *
     * @param peer        the peer
     * @param currentPeer the current peer
     */
    public void sendUpdatedInfoToPeer(Peer peer, Peer currentPeer) {
        hostSelectionInterceptor.setHost(Peer.getPeerUrl(peer));
        executeRequest(peerApi.updatePeer(currentPeer));
    }

    /**
     * The type Peer network service.
     */
    @Component
    public static class PeerNetworkService extends NetworkService {

        /**
         * The Peer service.
         */
        @Autowired
        PeerService peerService;

        /*
         * (non-Javadoc)
         *
         * @see com.hhbackend.network.NetworkService#getApiUrl()
         */
        @Override
        public String getApiUrl() {
            return Peer.getPeerUrl(peerService.getOwnPeer());
        }

    }

    /**
     * The interface Peer api.
     */
    public interface PeerApi {

        /**
         * Add peers call.
         *
         * @param queryParams the query params
         * @return the call
         */
        @POST("peers")
        Call<Void> addPeers(@Body Map<String, Object> queryParams);

        /**
         * Update peer call.
         *
         * @param peer the peer
         * @return the call
         */
        @PUT("peers")
        Call<Void> updatePeer(@Body Peer peer);

    }
}
