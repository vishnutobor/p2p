package com.p2p.files.models;

import com.p2p.constants.DBConstants;
import com.p2p.files.dao.FileDBConstants;
import com.p2p.model.AbstractDatabaseObject;
import com.p2p.model.BooleanStatus;
import com.p2p.model.marshaller.BooleanStatusMarshaller;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * The type Uploaded file chunk.
 */
@Entity
@Table(name = FileDBConstants.TABLE_FILE_CHUNKS,
        uniqueConstraints = {@UniqueConstraint(columnNames = {FileDBConstants.COLUMN_FILE_CHUNKS_MD5_HASH})})
public class UploadedFileChunk extends AbstractDatabaseObject {

    @Id
    @Column(name = FileDBConstants.COLUMN_FILE_CHUNKS_ID)
    @GeneratedValue(generator = DBConstants.HIBERNATE_UUID_GENERATOR)
    @GenericGenerator(name = DBConstants.HIBERNATE_UUID_GENERATOR,
            strategy = DBConstants.HIBERNATE_UUID_GENERATOR_STRATEGY)
    private String fileChunkId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = FileDBConstants.COLUMN_FILE_CHUNKS_FILE_ID,
            referencedColumnName = FileDBConstants.COLUMN_FILES_ID)
    private UploadedFile uploadedFile;
    @Column(name = FileDBConstants.COLUMN_FILE_CHUNKS_MD5_HASH)
    private String chunkHash;
    @Column(name = FileDBConstants.COLUMN_FILE_CHUNKS_OFFSET)
    private Long offset;
    @Column(name = FileDBConstants.COLUMN_FILE_CHUNKS_LENGTH)
    private Long size;
    @Column(name = FileDBConstants.COLUMN_FILE_CHUNKS_STATUS)
    @Convert(converter = BooleanStatusMarshaller.class)
    private BooleanStatus status;

    /**
     * Gets file chunk id.
     *
     * @return the file chunk id
     */
    public String getFileChunkId() {
        return fileChunkId;
    }

    /**
     * Sets file chunk id.
     *
     * @param fileChunkId the file chunk id
     */
    public void setFileChunkId(String fileChunkId) {
        this.fileChunkId = fileChunkId;
    }

    /**
     * Gets uploaded file.
     *
     * @return the uploaded file
     */
    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    /**
     * Sets uploaded file.
     *
     * @param uploadedFile the uploaded file
     */
    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    /**
     * Gets offset.
     *
     * @return the offset
     */
    public Long getOffset() {
        return offset;
    }

    /**
     * Sets offset.
     *
     * @param offset the offset
     */
    public void setOffset(Long offset) {
        this.offset = offset;
    }

    /**
     * Gets size.
     *
     * @return the size
     */
    public Long getSize() {
        return size;
    }

    /**
     * Sets size.
     *
     * @param size the size
     */
    public void setSize(Long size) {
        this.size = size;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public BooleanStatus getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(BooleanStatus status) {
        this.status = status;
    }
}
