package com.p2p.files.service;

import com.p2p.exceptions.CoreException;
import com.p2p.files.dao.UploadedFileChunkDao;
import com.p2p.files.dao.UploadedFileDao;
import com.p2p.files.models.UploadedFile;
import com.p2p.files.models.UploadedFileChunk;
import com.p2p.model.BooleanStatus;
import net.sf.oval.constraint.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The type Uploaded file service.
 */
@Service("uploadedFileService")
@Transactional(propagation = Propagation.SUPPORTS, rollbackFor = Throwable.class)
public class UploadedFileService {

    @Autowired
    private UploadedFileDao uploadedFileDao;
    @Autowired
    private UploadedFileChunkDao uploadedFileChunkDao;

    /**
     * Create uploaded file string.
     *
     * @param uploadedFile the uploaded file
     * @return the string
     */
    public String createUploadedFile(@NotNull(message = "uploaded file cannot be null") UploadedFile uploadedFile) {
        return (String) uploadedFileDao.create(uploadedFile);
    }

    /**
     * Gets uploaded file.
     *
     * @param uploadedFileId the uploaded file id
     * @return the uploaded file
     */
    public UploadedFile getUploadedFile(@NotNull(message = "uploaded file id cannot be null") String uploadedFileId) {
        UploadedFile file = uploadedFileDao.get(UploadedFile.class, uploadedFileId);
        if (file == null) {
            throw new CoreException.NotFoundException("file with id %s doesnt exist", uploadedFileId);
        }
        return file;
    }

    /**
     * Gets uploaded file by hash.
     *
     * @param fileHash the file hash
     * @return the uploaded file by hash
     */
    public UploadedFile getUploadedFileByHash(@NotNull(message = "file hash cannot be ull") String fileHash) {
        UploadedFile file = uploadedFileDao.getFileByHash(fileHash);
        if (file == null) {
            throw new CoreException.NotFoundException("file with hash %s doesnt exist", fileHash);
        }
        return file;
    }

    /**
     * Gets all uploaded files.
     *
     * @param status the status
     * @return the all uploaded files
     */
    public List<UploadedFile> getAllUploadedFiles(BooleanStatus status) {
        return uploadedFileDao.getAllFiles(status);
    }

    /**
     * Create uploaded file chunk string.
     *
     * @param uploadedFileChunk the uploaded file chunk
     * @return the string
     */
    public String createUploadedFileChunk(@NotNull(message = "uploaded file chunk cannot be null")
                                                  UploadedFileChunk uploadedFileChunk) {
        return (String) uploadedFileChunkDao.create(uploadedFileChunk);
    }

    /**
     * Gets uploaded file chunk.
     *
     * @param uploadedFileChunkId the uploaded file chunk id
     * @return the uploaded file chunk
     */
    public UploadedFileChunk getUploadedFileChunk(
            @NotNull(message = "uploaded file chunk id cannot be null") String uploadedFileChunkId) {
        UploadedFileChunk fileChunk = uploadedFileChunkDao.get(UploadedFileChunk.class, uploadedFileChunkId);
        if (fileChunk == null) {
            throw new CoreException.NotFoundException("file chunk with id %s doesnt exist", uploadedFileChunkId);
        }
        return fileChunk;
    }

    /**
     * Gets uploaded file chunk by hash.
     *
     * @param chunkHash the chunk hash
     * @return the uploaded file chunk by hash
     */
    public UploadedFileChunk getUploadedFileChunkByHash(
            @NotNull(message = "file hash cannot be null") String chunkHash) {
        UploadedFileChunk fileChunk = uploadedFileChunkDao.getFileChunkByHash(chunkHash, "uploadedFile");
        if (fileChunk == null) {
            throw new CoreException.NotFoundException("file chunk with hash %s doesnt exist", chunkHash);
        }
        return fileChunk;
    }

    /**
     * Gets uploaded file chunks by file.
     *
     * @param file the file
     * @return the uploaded file chunks by file
     */
    public List<UploadedFileChunk> getUploadedFileChunksByFile(
            @NotNull(message = "uploaded file cannot be null") UploadedFile file) {
        return uploadedFileChunkDao.getFileChunksByFile(file, "uploadedFile");
    }
}
