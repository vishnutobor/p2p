package com.p2p.files.dao;

import com.p2p.dao.AbstractDao;
import com.p2p.files.models.UploadedFile;
import com.p2p.files.models.UploadedFileChunk;
import com.p2p.utils.DateTimeUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * The type Uploaded file chunk dao.
 */
@Repository("uploadedFileChunkDao")
public class UploadedFileChunkDao extends AbstractDao {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private DateTimeUtils dateTimeUtils;

    @PostConstruct
    private void initializeDao() {
        setSessionFactory(sessionFactory);
        setDateTimeUtils(dateTimeUtils);
    }

    /**
     * Gets file chunks by file.
     *
     * @param uploadedFile the uploaded file
     * @param fetchClasses the fetch classes
     * @return the file chunks by file
     */
    @SuppressWarnings("unchecked")
    public List<UploadedFileChunk> getFileChunksByFile(UploadedFile uploadedFile, String... fetchClasses) {
        Session session = getCurrentSession();
        Criteria completeCriteria = session.createCriteria(UploadedFile.class);

        completeCriteria.add(Restrictions.eq("uploadedFile", uploadedFile));

        completeCriteria = setFetchClasses(completeCriteria, FetchMode.JOIN, fetchClasses);

        return completeCriteria.list();
    }

    /**
     * Gets file chunk by hash.
     *
     * @param hash         the hash
     * @param fetchClasses the fetch classes
     * @return the file chunk by hash
     */
    public UploadedFileChunk getFileChunkByHash(String hash, String... fetchClasses) {
        Session session = getCurrentSession();
        Criteria completeCriteria = session.createCriteria(UploadedFile.class);

        completeCriteria.add(Restrictions.eq("chunkHash", hash));

        completeCriteria = setFetchClasses(completeCriteria, FetchMode.JOIN, fetchClasses);

        return (UploadedFileChunk) completeCriteria.uniqueResult();
    }
}
