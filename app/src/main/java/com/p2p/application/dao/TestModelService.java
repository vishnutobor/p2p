package com.p2p.application.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.p2p.application.model.TestModel;

/**
 * The type Test model service.
 */
@Service("userRoutineEventService")
@Transactional(propagation = Propagation.SUPPORTS, rollbackFor = Throwable.class)
public class TestModelService {

    @Autowired
    private TestModelDao testModelDao;

    /**
     * Create test model string.
     *
     * @param model the model
     * @return the string
     */
    public String createTestModel(TestModel model) {
        return testModelDao.createTestModel(model);
    }

    /**
     * Gets test model.
     *
     * @param id the id
     * @return the test model
     */
    public TestModel getTestModel(String id) {
        return testModelDao.get(id);
    }
}
