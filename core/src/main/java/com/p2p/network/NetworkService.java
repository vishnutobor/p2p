package com.p2p.network;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * The Class NetworkService.
 */
public abstract class NetworkService {

    /**
     * The constant LOG.
     */
    protected static final Logger LOG = Logger.getLogger(NetworkService.class);

    /**
     * The Object mapper.
     */
    @Autowired
    @Qualifier("hhObjectMapper")
    protected ObjectMapper objectMapper;
    /**
     * The Host selection interceptor.
     */
    @Autowired
    protected HostSelectionInterceptor hostSelectionInterceptor;

    /**
     * The Logging interceptor.
     */
    protected HttpLoggingInterceptor loggingInterceptor;
    /**
     * The Http client.
     */
    protected OkHttpClient httpClient;
    /**
     * The Retrofit.
     */
    protected Retrofit retrofit;

    @PostConstruct
    private void initializeNetwork() {
        this.loggingInterceptor = provideHttpLoggingInterceptor();
        this.httpClient = provideOkHttpClient(this.loggingInterceptor, this.hostSelectionInterceptor);
        this.retrofit = provideRetrofit(this.objectMapper, getApiUrl(), this.httpClient);
    }

    /**
     * Provide http logging interceptor.
     *
     * @return the http logging interceptor
     */
    protected HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                LOG.debug(message);
            }
        });
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return loggingInterceptor;
    }

    /**
     * Provide ok http client.
     *
     * @param loggingInterceptor       the logging interceptor
     * @param hostSelectionInterceptor the host selection interceptor
     * @return the ok http client
     */
    protected OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor,
                                               HostSelectionInterceptor hostSelectionInterceptor) {
        OkHttpClient client = new OkHttpClient.Builder().
                addInterceptor(loggingInterceptor).
                addInterceptor(hostSelectionInterceptor)
                .build();
        return client;
    }

    /**
     * Provide retrofit.
     *
     * @param objectMapper the object mapper
     * @param baseApiUrl   the base api url
     * @param httpClient   the http client
     * @return the retrofit
     */
    protected Retrofit provideRetrofit(ObjectMapper objectMapper, String baseApiUrl, OkHttpClient httpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(baseApiUrl).client(httpClient).build();
        return retrofit;
    }

    /**
     * Gets the logging interceptor.
     *
     * @return the logging interceptor
     */
    public HttpLoggingInterceptor getLoggingInterceptor() {
        return loggingInterceptor;
    }

    /**
     * Gets the http client.
     *
     * @return the http client
     */
    public OkHttpClient getHttpClient() {
        return httpClient;
    }

    /**
     * Gets the retrofit.
     *
     * @return the retrofit
     */
    public Retrofit getRetrofit() {
        return retrofit;
    }

    /**
     * Gets the api url.
     *
     * @return the api url
     */
    public abstract String getApiUrl();
}
