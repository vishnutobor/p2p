package com.p2p.dao;

import java.io.Serializable;

import com.p2p.utils.DateTimeUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.p2p.model.AbstractDatabaseObject;

/**
 * The type Abstract dao.
 */
public class AbstractDao {

    private SessionFactory sessionFactory;
    private DateTimeUtils dateTimeUtils;

    /**
     * Gets the current session.
     *
     * @return the current session
     */
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    /**
     * Gets the.
     *
     * @param <T>        the generic type
     * @param entityType the entity type
     * @param id         the id
     * @return the t
     */
    public <T extends AbstractDatabaseObject> T get(Class<T> entityType, Serializable id) {
        Session session = getCurrentSession();
        T entity = session.get(entityType, id);
        return entity;
    }

    /**
     * Creates the.
     *
     * @param <T>    the generic type
     * @param entity the entity
     * @return the serializable
     */
    public <T extends AbstractDatabaseObject> Serializable create(T entity) {
        Session session = getCurrentSession();
        return session.save(entity);
    }

    /**
     * Update.
     *
     * @param <T>    the generic type
     * @param entity the entity
     */
    public <T extends AbstractDatabaseObject> void update(T entity) {
        Session session = getCurrentSession();
        session.update(entity);
    }

    /**
     * Save or update.
     *
     * @param <T>    the generic type
     * @param entity the entity
     */
    public <T extends AbstractDatabaseObject> void saveOrUpdate(T entity) {
        Session session = getCurrentSession();
        entity.setLastUpdatedTime(dateTimeUtils.getApplicationCurrentTime());
        session.saveOrUpdate(entity);
    }

    /**
     * Delete.
     *
     * @param <T>    the generic type
     * @param entity the entity
     */
    public <T extends AbstractDatabaseObject> void delete(T entity) {
        Session session = getCurrentSession();
        session.delete(entity);
    }

    /**
     * Gets session factory.
     *
     * @return the session factory
     */
    protected SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * Sets session factory.
     *
     * @param sessionFactory the session factory
     */
    protected void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Gets date time utils.
     *
     * @return the date time utils
     */
    protected DateTimeUtils getDateTimeUtils() {
        return dateTimeUtils;
    }

    /**
     * Sets date time utils.
     *
     * @param dateTimeUtils the date time utils
     */
    protected void setDateTimeUtils(DateTimeUtils dateTimeUtils) {
        this.dateTimeUtils = dateTimeUtils;
    }

    /**
     * Sets fetch classes.
     *
     * @param criteria      the criteria
     * @param fetchMode     the fetch mode
     * @param assosciations the assosciations
     * @return the fetch classes
     */
    protected Criteria setFetchClasses(Criteria criteria, FetchMode fetchMode, String... assosciations) {
        if (CollectionUtils.sizeIsEmpty(assosciations)) {
            return criteria;
        }
        for (String association : assosciations) {
            criteria.setFetchMode(association, fetchMode);
        }
        return criteria;
    }

}
