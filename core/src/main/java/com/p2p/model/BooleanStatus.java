package com.p2p.model;

/**
 * The Enum BooleanStatus.
 */
public enum BooleanStatus {

    /**
     * Active boolean status.
     */
    ACTIVE, /**
     * Disabled boolean status.
     */
    DISABLED, /**
     * Expired boolean status.
     */
    EXPIRED, /**
     * Pending boolean status.
     */
    PENDING, /**
     * Accept boolean status.
     */
    ACCEPT, /**
     * Reject boolean status.
     */
    REJECT, /**
     * Created boolean status.
     */
    CREATED, /**
     * Completed boolean status.
     */
    COMPLETED;
}
