package com.p2p.utils;

import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

/**
 * The type Dao utils.
 */
@Component
public class DaoUtils {

    /**
     * Gets the current time.
     *
     * @return the current time
     */
    public LocalDateTime getCurrentTime() {
        return LocalDateTime.now();
    }

    /**
     * Close silently.
     *
     * @param closeable the closeable
     */
    public void closeSilently(AutoCloseable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
                // LOG.debug(e.getMessage(), e);
            }
        }
    }

}
