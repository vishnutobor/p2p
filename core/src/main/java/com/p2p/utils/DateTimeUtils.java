package com.p2p.utils;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;

import com.p2p.exceptions.CoreException;
import org.springframework.stereotype.Component;


/**
 * The Class DateTimeUtils. Contains utility methods for date tine logic
 */
@Component
public class DateTimeUtils {

    /**
     * The constant DATE_YY_MM_DD_FORMAT.
     */
    public static final String DATE_YY_MM_DD_FORMAT = "yyyy-MM-DD";

    /**
     * The constant DATE_FORMATTER.
     */
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ISO_LOCAL_DATE;

    /**
     * The constant DATE_TIME_FORMATTER.
     */
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    /**
     * The constant ASIA_KOLKATA_ZONE_ID.
     */
    public static final ZoneId ASIA_KOLKATA_ZONE_ID = ZoneId.of("Asia/Kolkata");

    /**
     * The constant DEFAULT_DATE_TIME.
     */
    public static final LocalDateTime DEFAULT_DATE_TIME = LocalDateTime.of(1994, 12, 10, 12, 00);


    /**
     * Gets the date from string.
     *
     * @param dateString the date string
     * @return the date from string
     */
    public LocalDate getDateFromString(String dateString) {
        try {
            LocalDate localDate = LocalDate.parse(dateString, DATE_FORMATTER);
            return localDate;
        } catch (DateTimeParseException de) {
            throw new CoreException.NotValidException(String.format("Date must be of format %s", DATE_YY_MM_DD_FORMAT));
        }
    }

    /**
     * Gets the time from string.
     *
     * @param dateTimeString the date time string
     * @return the time from string
     */
    public LocalDateTime getTimeFromString(String dateTimeString) {
        try {
            LocalDateTime localDateTime = LocalDateTime.parse(dateTimeString, DATE_TIME_FORMATTER);
            return localDateTime;
        } catch (DateTimeParseException de) {
            throw new CoreException.NotValidException(
                    String.format("Time must be of format %s", DATE_TIME_FORMATTER.toString()), de);
        }
    }

    /**
     * Convert time to string.
     *
     * @param dateTime the date time
     * @return the string
     */
    public String convertTimeToString(LocalDateTime dateTime) {
        return convertTimeToString(dateTime, DATE_TIME_FORMATTER);
    }

    /**
     * Convert time to string.
     *
     * @param dateTime          the date time
     * @param dateTimeFormatter the date time formatter
     * @return the string
     */
    public String convertTimeToString(LocalDateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        try {
            return dateTime.format(dateTimeFormatter);
        } catch (Exception e) {
            throw new CoreException.NotValidException(e.getMessage(), e);
        }
    }

    /**
     * Convert time to string.
     *
     * @param dateTime the date time
     * @return the string
     */
    public String convertTimeToString(ZonedDateTime dateTime) {
        return convertTimeToString(dateTime, DATE_TIME_FORMATTER);
    }

    /**
     * Convert time to string.
     *
     * @param dateTime          the date time
     * @param dateTimeFormatter the date time formatter
     * @return the string
     */
    public String convertTimeToString(ZonedDateTime dateTime, DateTimeFormatter dateTimeFormatter) {
        try {
            return dateTime.format(dateTimeFormatter);
        } catch (Exception e) {
            throw new CoreException.NotValidException(e.getMessage(), e);
        }
    }

    /**
     * Gets the zoned date time.
     *
     * @param dateTime the date time
     * @return the zoned date time
     */
    public ZonedDateTime getZonedDateTime(LocalDateTime dateTime) {
        return getZonedDateTime(dateTime, ASIA_KOLKATA_ZONE_ID);
    }

    /**
     * Gets the application current time.
     *
     * @return the application current time
     */
    public LocalDateTime getApplicationCurrentTime() {
        ZonedDateTime dateTime = ZonedDateTime.now(ASIA_KOLKATA_ZONE_ID);
        return dateTime.toLocalDateTime();
    }

    /**
     * Gets the zoned date time.
     *
     * @param dateTime the date time
     * @param zoneId   the zone id
     * @return the zoned date time
     */
    public ZonedDateTime getZonedDateTime(LocalDateTime dateTime, ZoneId zoneId) {
        ZonedDateTime zonedDateTime = ZonedDateTime.of(dateTime, zoneId);
        return zonedDateTime;
    }

    /**
     * Gets the date time from date.
     *
     * @param date       the date
     * @param startOfDay the start of day
     * @return the date time from date
     */
    public ZonedDateTime getDateTimeFromDate(LocalDate date, boolean startOfDay) {
        ZoneId zoneId = ASIA_KOLKATA_ZONE_ID;
        if (startOfDay) {
            return date.atStartOfDay(zoneId);
        } else {
            return date.atTime(LocalTime.MAX).atZone(zoneId);
        }
    }

    /**
     * Gets the zoned date time from string.
     *
     * @param dateTimeString the date time string
     * @return the zoned date time from string
     */
    public ZonedDateTime getZonedDateTimeFromString(String dateTimeString) {
        try {
            return ZonedDateTime.parse(dateTimeString);
        } catch (Exception e) {
            throw new CoreException.NotValidException(e.getMessage(), e);
        }
    }

    /**
     * Gets the end time of local date.
     *
     * @param localDate the local date
     * @return the end time of local date
     */
    public LocalDateTime getEndTimeOfLocalDate(LocalDate localDate) {
        return localDate.atTime(LocalTime.MAX);
    }

    /**
     * Gets the start time of local date.
     *
     * @param localDate the local date
     * @return the start time of local date
     */
    public LocalDateTime getStartTimeOfLocalDate(LocalDate localDate) {
        return localDate.atStartOfDay();
    }

    /**
     * Gets the end time of zoned date time.
     *
     * @param zonedDateTime the zoned date time
     * @return the end time of zoned date time
     */
    public ZonedDateTime getEndTimeOfZonedDateTime(ZonedDateTime zonedDateTime) {
        return zonedDateTime.toLocalDate().atTime(LocalTime.MAX).atZone(ASIA_KOLKATA_ZONE_ID);
    }

    /**
     * Gets the start time of zoned date time.
     *
     * @param zonedDateTime the zoned date time
     * @return the start time of zoned date time
     */
    public ZonedDateTime getStartTimeOfZonedDateTime(ZonedDateTime zonedDateTime) {
        return zonedDateTime.toLocalDate().atStartOfDay(ASIA_KOLKATA_ZONE_ID);
    }

    /**
     * Gets the local time from local date.
     *
     * @param localDate    the local date
     * @param atStartOfDay the at start of day
     * @return the local time from local date
     */
    public LocalDateTime getLocalTimeFromLocalDate(LocalDate localDate, boolean atStartOfDay) {
        if (atStartOfDay) {
            return getStartTimeOfLocalDate(localDate);
        }
        return getEndTimeOfLocalDate(localDate);
    }

    /**
     * Between.
     *
     * @param startTime the start time
     * @param endTime   the end time
     * @param unit      the unit
     * @return the long
     */
    public long between(Temporal startTime, Temporal endTime, ChronoUnit unit) {
        return unit.between(startTime, endTime);
    }

    /**
     * Gets the local time.
     *
     * @param localTimeString the local time string
     * @return the local time
     */
    public LocalTime getLocalTime(String localTimeString) {
        try {
            return LocalTime.parse(localTimeString);
        } catch (Exception e) {
            throw new CoreException.NotValidException(e.getMessage(), e);
        }
    }

    /**
     * Copy time to local date time.
     *
     * @param localDateTime the local date time
     * @param localTime     the local time
     * @return the local date time
     */
    public LocalDateTime copyTimeToLocalDateTime(LocalDateTime localDateTime, LocalTime localTime) {
        return localTime.atDate(localDateTime.toLocalDate());
    }

    /**
     * Gets the epoch seconds.
     *
     * @param localDateTime the local date time
     * @return the epoch seconds
     */
    public long getEpochSeconds(LocalDateTime localDateTime) {
        return localDateTime.atZone(ASIA_KOLKATA_ZONE_ID).toEpochSecond();
    }

    /**
     * Format local date time.
     *
     * @param localDateTime the local date time
     * @param format        the format
     * @return the string
     */
    public String formatLocalDateTime(LocalDateTime localDateTime, String format) {
        return formatLocalDateTime(localDateTime, DateTimeFormatter.ofPattern(format));
    }

    /**
     * Format local date time.
     *
     * @param localDateTime     the local date time
     * @param dateTimeFormatter the date time formatter
     * @return the string
     */
    public String formatLocalDateTime(LocalDateTime localDateTime, DateTimeFormatter dateTimeFormatter) {
        return localDateTime.format(dateTimeFormatter);
    }

    /**
     * Format local date.
     *
     * @param localDate the local date
     * @param format    the format
     * @return the string
     */
    public String formatLocalDate(LocalDate localDate, String format) {
        return formatLocalDate(localDate, DateTimeFormatter.ofPattern(format));
    }

    /**
     * Format local date.
     *
     * @param localDate         the local date
     * @param dateTimeFormatter the date time formatter
     * @return the string
     */
    public String formatLocalDate(LocalDate localDate, DateTimeFormatter dateTimeFormatter) {
        return localDate.format(dateTimeFormatter);
    }

    /**
     * Gets the duration between.
     *
     * @param startTime the start time
     * @param endTime   the end time
     * @return the duration between
     */
    public Duration getDurationBetween(Temporal startTime, Temporal endTime) {
        return Duration.between(startTime, endTime);
    }

    /**
     * Format duration.
     *
     * @param duration the duration
     * @return the string
     */
    public String formatDuration(Duration duration) {
        return duration.toString();
    }
}
